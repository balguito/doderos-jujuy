#!/bin/bash

echo "Recorriendo directorios y generando hash de los archivos..."

find * -not -name "*.bz2" -not -name "md5sum.txt" -not -name "updateMD5sum.sh" -type f -exec md5sum '{}' \; > md5sum.txt

echo "Se cargaron $(wc -l md5sum.txt) archivos en md5sum.txt"
